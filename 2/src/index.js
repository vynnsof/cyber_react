import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App/App';
import StudentCard from './components/StudentCard/StudentCard'

ReactDOM.render(
  <React.StrictMode>
    <div className="cards-holder">
    <App /> 
      <StudentCard name="Shyam" text="I hope you enjoy learning React!" date="13.11.2021"/>
    </div>
  </React.StrictMode>,
  document.getElementById('root')
);
