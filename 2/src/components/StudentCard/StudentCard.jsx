import React from "react";
import './StudentCard.css'

class StudentCard extends React.Component{
   
    render(){

        const name = this.props.name;
        const text = this.props.text;
        const date = this.props.date;
        

        return (
            <div className="card">
                <div className="card-item-name">Student name: {name}</div>
                <div className="card-item-text">{text}</div>
                <div className="card-item-date">{date}</div>
            </div>
        )
    }
}
export default StudentCard;